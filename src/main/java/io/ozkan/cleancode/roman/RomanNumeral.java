package io.ozkan.cleancode.roman;

import java.util.HashMap;
import java.util.Map;

public class RomanNumeral {

	private static final Map<String, Integer> DIGITS =
			new HashMap<>();
	
	static {
		DIGITS.put("I", 1);
		DIGITS.put("V", 5);
		DIGITS.put("X", 10);
	}
	
	public int convert(String input) {
		if(!DIGITS.containsKey(input)) {
			throw new UnknownDigit(input);
		}
		return DIGITS.get(input);
	}

	public static class UnknownDigit extends RuntimeException {
		private UnknownDigit(String digit) {
			super(digit + " is unknown");
		}
	}
}
