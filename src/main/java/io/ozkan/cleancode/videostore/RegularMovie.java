package io.ozkan.cleancode.videostore;

public class RegularMovie extends Movie {

	public RegularMovie(String title) {
		super(title);
	}

	@Override
	protected double determineAmount(int daysRented) {
		double thisAmount = 2;
		if (daysRented > 2)
			thisAmount += (daysRented - 2) * 1.5;
		return thisAmount;
	}

	@Override
	protected int determineFrequentRenterPoints(int daysRented) {
		return 1;
	}

}
