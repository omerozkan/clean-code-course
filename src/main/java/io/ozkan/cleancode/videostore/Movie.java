package io.ozkan.cleancode.videostore;

public abstract class Movie {
    private String title;

    public Movie(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

	protected abstract double determineAmount(int daysRented);

	protected abstract int determineFrequentRenterPoints(int daysRented);
}