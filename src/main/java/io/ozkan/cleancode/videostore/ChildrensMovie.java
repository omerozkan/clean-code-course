package io.ozkan.cleancode.videostore;

public class ChildrensMovie extends Movie {

	public ChildrensMovie(String title) {
		super(title);
	}

	@Override
	protected double determineAmount(int daysRented) {
		double thisAmount = 1.5;
		if (daysRented > 3)
			thisAmount += (daysRented - 3) * 1.5;
		return thisAmount;
	}

	@Override
	protected int determineFrequentRenterPoints(int daysRented) {
		return 1;
	}

}
