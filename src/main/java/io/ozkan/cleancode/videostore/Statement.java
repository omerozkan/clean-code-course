package io.ozkan.cleancode.videostore;

import java.util.ArrayList;
import java.util.List;

public class Statement {

	private List<Rental> rentals = new ArrayList<>();
	private String customerName;

	private double totalAmount;
	private int frequentRenterPoints;

	public Statement(String customerName) {
		this.customerName = customerName;
	}

	public String makeStatement() {
		resetTotalAmountAndRenterPoints();
		return makeHeader() + makeRentalLines() + makeFooter();
	}

	private void resetTotalAmountAndRenterPoints() {
		totalAmount = 0;
		frequentRenterPoints = 0;
	}

	private String makeHeader() {
		return "Rental Record for " + getCustomerName() + "\n";
	}

	private String makeRentalLines() {
		StringBuilder sb = new StringBuilder();
		for (Rental each : this.rentals) {
			sb.append(makeEachLine(each));
		}
		return sb.toString();
	}


	private String makeEachLine(Rental rental) {
		double thisAmount = rental.determineAmount();
		totalAmount += thisAmount;
		
		frequentRenterPoints += rental.determineFrequentRenterPoints();

		return String.format("\t%s\t%.1f\n", rental.getMovieTitle(), thisAmount);
	}
	
	private String makeFooter() {
		return String.format("Amount owed is %.1f\n" 
				+ "You earned %d frequent renter points", totalAmount, frequentRenterPoints);
	}

	public void addRental(Rental rental) {
		rentals.add(rental);
	}

	public List<Rental> getRentals() {
		return rentals;
	}

	public void setRentals(List<Rental> rentals) {
		this.rentals = rentals;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String name) {
		this.customerName = name;
	}

	public double getTotalAmount() {
		return this.totalAmount;
	}

	public int getFrequentRenterPoints() {
		return this.frequentRenterPoints;
	}

}