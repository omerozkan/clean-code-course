package io.ozkan.cleancode.functions.price;

/**
 * @author Omer Ozkan
 * @version 29/01/16
 */
public class PriceCalculator {

    public static final double TAX_RATE_FOR_DE = 19.00;
    public static final double TAX_RATE_FOR_TR = 18.00;
    public static final double TAX_RATE_FOR_AT = 20.00;

    public Double calculatePrice(final String locale, final double price) {
        if (locale == null) {
            return 0d;
        }

        Double result = 0d;

        if (isLocalGermany(locale)) {
            result = calculatePrice(price, TAX_RATE_FOR_DE);
        } else if (isLocaleTurkey(locale)) {
            result = calculatePrice(price, TAX_RATE_FOR_TR);
        } else if (isLocaleAustria(locale)) {
            result = calculatePrice(price, TAX_RATE_FOR_AT);
        }

        return result;
    }

    private boolean isLocaleAustria(String locale) {
        return locale.equals("at_AT");
    }

    private boolean isLocaleTurkey(String locale) {
        return locale.equals("tr_TR");
    }

    private boolean isLocalGermany(String locale) {
        return locale.equals("de_DE");
    }

    private double calculatePrice(double price, double taxRate) {
        return price + (price / 100) * taxRate;
    }

}