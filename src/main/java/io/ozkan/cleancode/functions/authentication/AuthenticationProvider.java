package io.ozkan.cleancode.functions.authentication;

import io.ozkan.cleancode.functions.authentication.entity.User;
import io.ozkan.cleancode.functions.authentication.service.LoginService;

import java.util.Map;

/**
 * @author Omer Ozkan
 * @version 26/01/16
 */
public class AuthenticationProvider {

    private static final String LOGIN_WITH_ADMIN_KEY = "loginWithAdmin";
    public static final String LOGIN_WITH_MOBILE_KEY = "mobile";

    private LoginService loginService;

    public AuthenticationProvider(LoginService loginService) {
        this.loginService = loginService;
    }

    public void checkAuthentication(Authentication authentication) throws AuthenticationException {
        checkAuthenticationIsNotEmpty(authentication);

        Map<String, String> params = authentication.getParameters();

        if (params.containsKey(LOGIN_WITH_ADMIN_KEY)) {
            tryAdminLogin(authentication);
        }

        if (params.containsKey(LOGIN_WITH_MOBILE_KEY)) {
            tryMobileLogin(authentication);
        }

        tryLoginWithUsernamePassword(authentication);
    }

    private void checkAuthenticationIsNotEmpty(Authentication authentication) throws AuthenticationException {
        if (isEmpty(authentication.getPrinciple())) {
            throw new AuthenticationException("Username cannot be empty");
        }

        if (isEmpty(authentication.getCredentials())) {
            throw new AuthenticationException("Password/token cannot be empty");
        }
    }

    private boolean isEmpty(String string) {
        return string == null || string.isEmpty();
    }

    private void tryAdminLogin(Authentication authentication) throws AuthenticationException {
        String[] adminCredentials = authentication.getCredentials().split("//");
        if (adminCredentials.length != 2) {
            loginService.logWrongAttempt(authentication);
            throw new AuthenticationException("Invalid username or password");
        }


        String adminUsername = adminCredentials[0];
        String adminPassword = adminCredentials[1];

        User user = loginService.getUser(adminUsername);
        if (user == null) {
            throw new AuthenticationException("User not found");
        }

        if (!user.getPassword().equals(adminPassword)) {
            throw new AuthenticationException("Bad credentials");
        }
        loginService.logAuthenticationWithAdminCredentials(authentication.getPrinciple(), adminUsername);
    }

    private void tryMobileLogin(Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrinciple();
        String credentials = authentication.getCredentials();

        boolean isMobileTokenValid = loginService.isMobileTokenValid(username, credentials);
        if (!isMobileTokenValid) {
            throw new AuthenticationException("Invalid token");
        }
        loginService.logLoginWithMobileToken(username, credentials);
    }

    private void tryLoginWithUsernamePassword(Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrinciple();
        String credentials = authentication.getCredentials();

        User user = loginService.getUser(username);
        checkUserPasswordIsCorrect(credentials, user);
        checkUserPasswordExpiration(username, credentials);
    }

    private void checkUserPasswordIsCorrect(String givenPassword, User user) throws AuthenticationException {
        //check password
        if (!user.getPassword().equals(givenPassword)) {
            throw new AuthenticationException("Invalid password");
        }
    }

    private void checkUserPasswordExpiration(String username, String credentials) throws PasswordHasExpiredException {
        boolean hasPasswordExpired = loginService.hasPasswordExpired(username, credentials);
        if (hasPasswordExpired) {
            throw new PasswordHasExpiredException("You have to change password to continue");
        }
    }

}
