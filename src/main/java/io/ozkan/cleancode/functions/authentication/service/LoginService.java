package io.ozkan.cleancode.functions.authentication.service;

import io.ozkan.cleancode.functions.authentication.Authentication;
import io.ozkan.cleancode.functions.authentication.entity.User;

/**
 * @author Omer Ozkan
 * @version 26/01/16
 */
public interface LoginService {

    void logWrongAttempt(Authentication authentication);

    void logAuthenticationWithAdminCredentials(String username, String adminUsername);

    void logLoginWithMobileToken(String username, String credentials);

    User getUser(String username);

    boolean hasPasswordExpired(String username, String credentials);

    boolean isMobileTokenValid(String username, String credentials);
}
