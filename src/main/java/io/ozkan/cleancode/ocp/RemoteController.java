package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class RemoteController {

    public void on(Device device) {
        device.on();
    }

    public void off(Device device) {
        device.off();
    }
}
