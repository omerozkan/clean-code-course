package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
interface Device {

    void on();

    void off();
}
