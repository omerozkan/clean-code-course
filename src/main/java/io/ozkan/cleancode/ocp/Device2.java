package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public interface Device2 extends Device {
    void vUp();
    void vDown();
}
