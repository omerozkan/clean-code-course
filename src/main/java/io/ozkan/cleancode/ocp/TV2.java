package io.ozkan.cleancode.ocp;

/**
 * @author Omer Ozkan
 */
public class TV2 implements Device2 {

    @Override
    public void on() {
        System.out.println("TV2 Turned On");
    }

    @Override
    public void off() {
        System.out.println("TV2 Turned Off");
    }

    @Override
    public void vUp() {
        System.out.println("TV2 Volume Increased");
    }

    @Override
    public void vDown() {
        System.out.println("TV2 Volume Decreased");
    }
}
