package io.ozkan.cleancode.lsp;

/**
 * @author Omer Ozkan
 */
public class RectangleTest {

    public static void doubleSize(Rectangle rectangle) {
        //PLease consider about square
        int x = rectangle.getX();
        rectangle.setX(2 * x);

        int y = rectangle.getY();
        rectangle.setY(2 * y);
    }

    public static void main(String[] args) {
        Rectangle r = new Rectangle();
        r.setX(3);
        r.setY(4);

        System.out.println("Rectangle: " + r);

        doubleSize(r);

        System.out.println("After doubled: " + r);

        Square s = new Square();
        s.setX(4);

        System.out.println("Square " + s);

        doubleSize(s);

        System.out.println("After doubled: " + s);
    }
}
