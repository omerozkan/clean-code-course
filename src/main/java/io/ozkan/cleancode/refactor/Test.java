package io.ozkan.cleancode.refactor;

import java.util.ArrayList;

/**
 * @author Omer Ozkan
 */
public class Test {
    class Hesap implements IHesap {
        ArrayList<AralikModel> AralikListesi;

        public Hesap() {
            AralikListesi = new ArrayList<>();

            //Soruda 100l¸k sistem olup olmad˝˝ yazmad˝˝ndan min va max deerleri kullan˝lm˝˛t˝r.
            AralikListesi.add(new AralikModel("A", 90, Integer.MAX_VALUE));
            AralikListesi.add(new AralikModel("B", 80, 89));
            AralikListesi.add(new AralikModel("C", 70, 79));
            AralikListesi.add(new AralikModel("D", Integer.MIN_VALUE, 70));
        }

        @Override
        public String classify(int score) {
            for (int i = 0; i < AralikListesi.size(); i++) {
                AralikModel secili = AralikListesi.get(i);
                if (secili.Araliktami(score)) {
                    return secili.getHarf();
                }
            }
            return "Hata";
        }
    }

    interface IHesap {
        String classify(int score);
    }

    class AralikModel {

        String Harf;
        int AralikBaslangic;
        int AralikBitis;


        public AralikModel(String _harf, int _aralikBaslangic, int _aralikBitis) {
            this.Harf = _harf;
            this.AralikBaslangic = _aralikBaslangic;
            this.AralikBitis = _aralikBitis;
        }

        //Hesap Kontrol
        public boolean Araliktami(int score) {
            return this.AralikBaslangic <= score && this.AralikBitis >= score;
        }

        public String getHarf() {
            return Harf;
        }
    }
}
