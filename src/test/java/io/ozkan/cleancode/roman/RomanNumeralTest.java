package io.ozkan.cleancode.roman;

import static org.junit.Assert.*;

import org.junit.Test;

public class RomanNumeralTest {

	private final RomanNumeral roman = new RomanNumeral();

	@Test
	public void convertIToOne() throws Exception {
		assertEquals(1, roman.convert("I"));
	}
	
	@Test
	public void convertVToFive() throws Exception {
		assertEquals(5, roman.convert("V"));
	}

	@Test
	public void convertXToTen() throws Exception {
		assertEquals(10, roman.convert("X"));
	}
	
	@Test(expected = RomanNumeral.UnknownDigit.class)
	public void throwExceptionWhenZIsGiven() throws Exception {
		roman.convert("Z");
	}
}
