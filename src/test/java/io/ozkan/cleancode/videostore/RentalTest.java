package io.ozkan.cleancode.videostore;

import io.ozkan.cleancode.videostore.Statement;
import io.ozkan.cleancode.videostore.Movie;
import io.ozkan.cleancode.videostore.Rental;
import org.junit.Test;

import static org.junit.Assert.*;


public class RentalTest {

	private Statement customer = new Statement("Customer");
	
	private Movie newReleaseMovie1 = new NewReleaseMovie("Rambo");
	private Movie newReleaseMovie2 = new NewReleaseMovie("Rambo2");
	private Movie childrensMovie = new ChildrensMovie("Winipoo");
	private Movie regularMovie1 = new RegularMovie("Winipoa");
	private Movie regularMovie2 = new RegularMovie("Winipoa2");

	@Test
	public void whenRentedANewRelase_AmountShouldBe_9_0() throws Exception {
		customer.addRental(new Rental(newReleaseMovie1, 3));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(9.0, 2);
	}

	private void assertTotalAmountAndFrequentRenterPointsCorrect(double expectedAmount, int expectedRenterPoints) {
		assertEquals(expectedAmount, customer.getTotalAmount(), 0);
		assertEquals(expectedRenterPoints, customer.getFrequentRenterPoints());
	}

	@Test
	public void whenRentedANewRelase_AmountShouldBe_3_0() throws Exception {
		customer.addRental(new Rental(newReleaseMovie1, 1));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(3.0, 1);
	}

	@Test
	public void whenRentedTwoNewRelase_AmountShouldBe_18_0() throws Exception {
		customer.addRental(new Rental(newReleaseMovie1, 3));
		customer.addRental(new Rental(newReleaseMovie2, 3));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(18.0, 4);
	}

	@Test
	public void whenRentedAChildrenMovie_AmountShouldBe_1_5() throws Exception {
		customer.addRental(new Rental(childrensMovie, 2));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(1.5, 1);
	}

	@Test
	public void whenRentedAChildrenMovieForFourDays_AmountShouldBe_1_5() throws Exception {
		customer.addRental(new Rental(childrensMovie, 4));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(3.0, 1);
	}

	@Test
	public void whenRentedARegular_AmountShouldBe_2_0() throws Exception {
		customer.addRental(new Rental(regularMovie1, 1));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(2.0, 1);
	}

	@Test
	public void whenRentedARegularMovieForSixDays_AmountShouldBe_8_0() throws Exception {
		customer.addRental(new Rental(regularMovie1, 6));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(8.0, 1);
	}

	@Test
	public void testWhenRentedTwoRegularMoviesForSixDays_AmountShouldBe_16_0() throws Exception {
		customer.addRental(new Rental(regularMovie1, 6));
		customer.addRental(new Rental(regularMovie2, 6));
		customer.makeStatement();
		assertTotalAmountAndFrequentRenterPointsCorrect(16.0, 2);
	}
	
	@Test
	public void statementFormat() throws Exception {
		customer.addRental(new Rental(regularMovie1, 6));
		customer.addRental(new Rental(regularMovie2, 6));
		assertEquals("" + "Rental Record for Customer\n" + "\tWinipoa\t8.0\n" + "\tWinipoa2\t8.0\n"
				+ "Amount owed is 16.0\n" + "You earned 2 frequent renter points", customer.makeStatement());
	}

}
